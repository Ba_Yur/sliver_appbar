import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            floating: true,
            leading: IconButton(
              onPressed: () {},
              icon: Icon(Icons.menu),
            ),
            expandedHeight: 100,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              background: Image.network(
                'https://www.rgo.ru/sites/default/files/styles/head_image_article/public/node/38230/kabancev-yurii-zerkalo-2018-379938.jpg?itok=llNijpJA',
                fit: BoxFit.cover,
              ),
              title: Text(
                'Centred title',
              ),
              stretchModes: [StretchMode.zoomBackground],
            ),
            backgroundColor: Colors.blue,
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                  height: 300,
                  color: Colors.green,
                  margin: EdgeInsets.all(20),
                ),
                Container(
                  height: 300,
                  color: Colors.green,
                  margin: EdgeInsets.all(20),
                ),
                Container(
                  height: 300,
                  color: Colors.green,
                  margin: EdgeInsets.all(20),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
